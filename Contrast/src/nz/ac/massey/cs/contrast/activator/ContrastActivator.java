package nz.ac.massey.cs.contrast.activator;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class ContrastActivator implements BundleActivator{

	@Override
	public void start(BundleContext arg0) throws Exception {
		System.out.println("start contrast");
		
	}

	@Override
	public void stop(BundleContext arg0) throws Exception {
		System.out.println("stop contrast");
		
	}

}

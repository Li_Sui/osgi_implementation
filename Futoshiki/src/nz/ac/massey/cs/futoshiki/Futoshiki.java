package nz.ac.massey.cs.futoshiki;

import java.awt.Color;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import nz.ac.massey.cs.contrast.Contrast;
import nz.ac.massey.cs.futoshiki.persistence.FutoshikiReader;
import nz.ac.massey.cs.model.Cell;
import nz.ac.massey.cs.model.Snapshot;

public class Futoshiki implements Contrast {
	
	private JPanel pane;
	private JLabel[][] labels = new JLabel[9][9];
	@Override
	public JPanel changeBoardLayout() {
	   pane=new JPanel();
	   pane.setOpaque(true);
       pane.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
       pane.setBackground(Color.WHITE);
		pane.setLayout(new java.awt.GridLayout(9, 9));
        for (int i=0;i<9;i++) {
        	for (int j=0;j<9;j++) {
        		JPanel p = new JPanel();
        		p.setOpaque(false);
        		p.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
                JLabel l = new JLabel();
                l.setOpaque(false);
                l.setHorizontalAlignment(SwingConstants.HORIZONTAL);
                l.setBorder(BorderFactory.createLineBorder(Color.GREEN,1));
                p.add(l);        		
               
                labels[i][j] = l;
                pane.add(p);
        	}
        }
		return pane;
	}

	@Override
	public JLabel[][] changeLabel() {
    	for(int i=0;i<9;i++){
    		if(i%2==0){
    			for(int j=0;j<9;j++){
    				if(j%2==0){
            			labels[i][j].setText(" ");
    				}
        		}
    		}
    	}
    	
		return this.labels;
	}

	@Override
	public Snapshot loadFile(File file) {

		FutoshikiReader r=new FutoshikiReader();
		Snapshot s=null;
		try {
			if(r.check(file)){
				s=r.read(file);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return s;
	}

	@SuppressWarnings("unused")
	@Override
	public Snapshot getNext(Snapshot arg0) {
		Iterator< Cell> emptyCell=arg0.unsolvedCells();
		Vector<Integer>[][] possibleValue=new Vector[9][9];
	   for(int i=1;i<10;i++){
			for(int j=1;j<10;j++){
				  possibleValue[i-1][j-1]=new Vector<Integer>(); // create an array in each coordinate 				
			}
		}
	   while(emptyCell.hasNext()){
           Cell temp = emptyCell.next();
			int RowNo=temp.getRow();
			int ColNo=temp.getCol();
			for(int k=1;k<6;k++){
				boolean findPvalue=true;
				Iterator<Cell> iterateRow=arg0.cellsByRow(RowNo);
				Iterator<Cell> iterateCol=arg0.cellsByColumn(ColNo);
				
				//iterate each row
			   while(iterateRow.hasNext()){
			    	Cell row = iterateRow.next();
			    	if(!row.getValue().equals("0") && row.getValue().equals(String.valueOf(k))){// if cell in row has value and the value is k
			    		                                       //statement is true, but findPossibleValue is false
			    		findPvalue=false;// not found
			    	}
			    }
			   //iterate each col
			    while(iterateCol.hasNext()){
			    	Cell col = iterateCol.next();
			    	if(!col.equals("0") && col.getValue().equals(String.valueOf(k))){// if cell in col has value and the value is k
			    		                                       //statement is true, but findPossibleValue is false
			    		findPvalue=false;// not found
			    	}
			    		
			    }
			    
			    
			    if(findPvalue==true){// find possible values
			    	
				   possibleValue[ColNo-1][RowNo-1].addElement(k);
			    }
			}
	   }
	   Iterator< Cell> check=arg0.unsolvedCells();
       while(check.hasNext()){
       	Cell temp2 = check.next();
			int RowNo2=temp2.getRow();
			int ColNo2=temp2.getCol();
				if(possibleValue[ColNo2-1][RowNo2-1].size()==1){// if we find there is only one possible value 
					for(int i=0;i<possibleValue[ColNo2-1][RowNo2-1].size();i++){
						if(possibleValue[ColNo2-1][RowNo2-1]!=null ){
					      temp2.setValue(String.valueOf(possibleValue[ColNo2-1][RowNo2-1].get(i)));// we set it in corresponding cell
					      if(arg0.IneqalityPosition().containsKey(temp2) && Integer.parseInt(temp2.getValue())>Integer.parseInt(arg0.IneqalityPosition().get(temp2).getValue())){
					    	  temp2.setValue("0");
					      }
						}
					}
			}
      }
       
		return arg0;
	}


}

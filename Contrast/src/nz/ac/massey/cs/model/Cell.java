package nz.ac.massey.cs.model;


public class Cell extends Annotatable{
	/**
	 * Constructor.
	 * @param col
	 * @param row
	 * @param value
	 */
	public Cell(int col, int row, String value) {
		super();
		this.col = col;
		this.row = row;
		this.value = value;
	}
	public Cell(int col, int row) {
		super();
		this.col = col;
		this.row = row;
		this.value = "0";
	}
	private int col = 0; // x position in the grid, between 1 and 9
	private int row = 0; // y position in the grid, between 1 and 9
	private String value = "0"; 
	

	public int getCol() {
		return col;
	}
	public void setCol(int col) {

		this.col = col;
	}

	public int getRow() {
		return row;
	}
	public void setRow(int row) {

		this.row = row;
	}

	public String getValue() {
		return value;
	}
	public void setValue(String value) {

		this.value = value;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + col;
		result = prime * result + row;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cell other = (Cell) obj;
		if (col != other.col)
			return false;
		if (row != other.row)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return new StringBuffer()
			.append(this.getClass().getName())
			.append('(')
			.append("x=")
			.append(this.getCol())
			.append(",y=")
			.append(this.getRow())
			.append(",value=")
			.append(this.getValue())
			.append(')')
			.toString();
	}
	@Override
	public Cell clone() {
		return new Cell(this.getCol(),this.getRow(),this.getValue());
	}
	
	
}

package nz.ac.massey.cs.model;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Snapshot extends Annotatable {
	private Map<Point,Cell> cells = new TreeMap<Point,Cell>(
			new Comparator<Object>() {
				@Override
				public int compare(Object arg0, Object arg1) {
					return arg0.toString().compareTo(arg1.toString());
				}
				
			}
	);
	public Snapshot() {
		super();
		for (int i=1;i<10;i++) {
			for (int j=1;j<10;j++) {
				cells.put(new Point(i,j),new Cell(i,j));
			}
		}
	}
	
	public void set(Cell Cell) {
		cells.put(new Point(Cell.getCol(),Cell.getRow()),Cell);
	}
	public Cell get(int col,int row) {
		return cells.get(new Point(col,row));
	}
	
	
	public Iterator<Cell> cellsByRow(final int row) {

		List<Cell> r =new ArrayList<Cell>();
		for(Map.Entry<Point, Cell> cell :cells.entrySet()){
			if(cell.getValue().getRow()==row){		
				r.add(cell.getValue());
			}
		}
		return r.iterator();
	}
	
	public Iterator<Cell> cellsByColumn(final int col) {

		List<Cell> column =new ArrayList<Cell>();
		for(Map.Entry<Point, Cell> cell :cells.entrySet()){
			if(cell.getValue().getCol()==col){
				column.add(cell.getValue());
			}
		}
		return column.iterator();
	}
	
	public Map<Cell,Cell> IneqalityPosition(){
		Map<Cell,Cell> result= new HashMap<Cell,Cell>();
		
		for(Map.Entry<Point, Cell> cell :cells.entrySet()){
			if(cell.getValue().getValue().equals("^") ){
				result.put(get(cell.getValue().getCol()-1,cell.getValue().getRow()),get(cell.getValue().getCol()+1 ,cell.getValue().getRow()));
			}
			if(cell.getValue().getValue().equals("<")){
				result.put(get(cell.getValue().getCol(),cell.getValue().getRow()-1),get(cell.getValue().getCol(),cell.getValue().getRow()+1));
			}
			if(cell.getValue().getValue().equals(">") ){
				result.put(get(cell.getValue().getCol(),cell.getValue().getRow()+1),get(cell.getValue().getCol(),cell.getValue().getRow()-1));
			}
			if(cell.getValue().getValue().equals("v") ){
				result.put(get(cell.getValue().getCol()+1 ,cell.getValue().getRow()),get(cell.getValue().getCol()-1,cell.getValue().getRow()));
			}

		}
		return result;
	}
	
	public Iterator<Cell> cellsByGroup(final int col,final int row) {
		List<Cell> group =new ArrayList<Cell>();
		int MINX = 3*(col-1);
		int MAXX = 3*col+1;
		int MINY = 3*(row-1);
		int MAXY = 3*row+1;
		for(Map.Entry<Point, Cell> cell :cells.entrySet()){
			int i = cell.getValue().getCol();
			int j = cell.getValue().getRow();
			if(i>MINX && i<MAXX && j>MINY && j<MAXY){
				group.add(cell.getValue());
			}
		}
		
		return group.iterator();
	}
	
	public Iterator<Cell> unsolvedCells() {
		List<Cell> unsolved =new ArrayList<Cell>();
		for(Map.Entry<Point, Cell> cell :cells.entrySet()){
			if(cell.getValue().getValue().equals("0")){
				unsolved.add(cell.getValue());
			}
		}
		return unsolved.iterator();
	}
	
	public Iterator<Cell> cells() {
		return this.cells.values().iterator();
	}
	// make a deep copy of this snapshot
	public Snapshot clone() {
		Snapshot clone = new Snapshot();
		Iterator<Cell> cells = this.cells();
		while (cells.hasNext()) {
			clone.set(cells.next().clone());
		}
		return clone;
	}
	
}

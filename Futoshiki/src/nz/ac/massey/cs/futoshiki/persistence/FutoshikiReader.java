package nz.ac.massey.cs.futoshiki.persistence;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.StringTokenizer;

import nz.ac.massey.cs.model.Cell;
import nz.ac.massey.cs.model.Snapshot;

public class FutoshikiReader {
	
	public boolean check(File file) throws IOException{
		LineNumberReader reader = new LineNumberReader(new FileReader(file));
		boolean isFutoshiki =false;
		String line = null;
		while ((line = reader.readLine())!=null) {
			for (StringTokenizer tok=new StringTokenizer(line,",");tok.hasMoreTokens();) {
				String token = tok.nextToken().trim();
				if(token.equals("<") || token.equals(">") || token.equals("^") || token.equals("v")){
					isFutoshiki=true;
					return isFutoshiki;
				}
			}
		}
		return isFutoshiki;
	}
	public Snapshot read(File file) throws IOException {
		Snapshot s = new Snapshot();
		@SuppressWarnings("resource")
		LineNumberReader reader = new LineNumberReader(new FileReader(file));
		String line = null;
		while ((line = reader.readLine())!=null) {
			int l = reader.getLineNumber();
			if( l%2 !=0 ){
				int c = 0;
				for (StringTokenizer tok=new StringTokenizer(line,",");tok.hasMoreTokens();) {
					c = c+1;
					if(c%2 !=0){
						String token = tok.nextToken().trim();
						String v="0";
						if(token.length()>0){
							v=token;
						}
						Cell cell = new Cell(c,l,v);
						s.set(cell);
					}else{
						String token = tok.nextToken().trim();
						String v="null";
						if(token.length()>0){
							v=token;
						}
						Cell cell = new Cell(c,l,v);
						s.set(cell);
					}
				
				}
			}else{
				int c = 0;
				for (StringTokenizer tok=new StringTokenizer(line,",");tok.hasMoreTokens();) {
					c = c+1;

					String token = tok.nextToken().trim();
					String v="null";
					if(token.length()>0){
						v=token;
					}
					Cell cell = new Cell(c,l,v);
					s.set(cell);
					
				}
			}	
		}
		return s;
	}
	
}	

package nz.ac.massey.cs.contrast;

import java.io.File;

import javax.swing.JLabel;
import javax.swing.JPanel;

import nz.ac.massey.cs.model.Snapshot;

/**
 * 
 * @author Li Sui
 *
 */
public interface Contrast {
	
	JPanel changeBoardLayout();
	JLabel[][] changeLabel();
	Snapshot loadFile(File file);
	Snapshot getNext(Snapshot current); 
	
}

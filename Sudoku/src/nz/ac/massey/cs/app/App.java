/*
 * SudokuApp.java
 *
 * Created on 9 February 2009, 20:17
 */

package nz.ac.massey.cs.app;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Iterator;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

import nz.ac.massey.cs.contrast.Contrast;
import nz.ac.massey.cs.model.Cell;
import nz.ac.massey.cs.model.Snapshot;
import nz.ac.massey.cs.sudoku.Sudoku;

/**
 * 
 * @author Li Sui
 *
 */

@SuppressWarnings("rawtypes")
public class App extends javax.swing.JFrame implements ServiceTrackerCustomizer {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5988024329035495562L;
	private static JLabel[][] currentLabels;
	private static JPanel  currentPane;
	private static JPanel mainPane ;
	private JToolBar toolbar = new JToolBar();
	private JSlider slider = new JSlider();
	private Action actPlay = null;
	private Action actLoad = null;
	private Action actStop = null;
	private Contrast futoshiki;
	private final BundleContext context;
	
	// model
	private Snapshot snapshot = null;
	private boolean animationOn = false;
	
	// misc
	private Timer timer = null;
	
    /** Creates new form SudokuApp */
    public App(BundleContext context) {
    	this.context=context;
    	initComponents();
    	 
    }
    
    private void setFutoshiki(final Contrast f){
    	 this.futoshiki=f;
    	 SwingUtilities.invokeLater(new Runnable() {
             @Override
             public void run() {
            	 if(currentPane!=null){
             		App.mainPane.remove(currentPane);
             		clearDisplay();
             	}
            	currentPane=f.changeBoardLayout();
            	App.currentLabels=f.changeLabel();
        	 	App.mainPane.add(currentPane,BorderLayout.CENTER);
        	 	App.mainPane.validate();
        	 	App.mainPane.repaint();
            	 
             }
    	 });
    }
    
    private void setSudoku(){
    	this.futoshiki=new Sudoku();
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                	if(currentPane!=null){
                		App.mainPane.remove(currentPane);
                		clearDisplay();
                	}
                	currentPane=futoshiki.changeBoardLayout();
                	currentLabels=futoshiki.changeLabel();
                	App.mainPane.add(currentPane,BorderLayout.CENTER);
                   	App.mainPane.validate();
                   	App.mainPane.repaint();
                   	//updateDisplay(futoshiki.loadFile());
                }
       	 });
        
    }
    
    /**
     * init application display
     */
    private void initComponents() {
    	this.setTitle("OSGI_Implementation");
    	mainPane = new JPanel(new BorderLayout());
    	setContentPane(mainPane);
   
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(480,600);
        this.setLocation((dim.width-this.getSize().width)/2, (dim.height-this.getSize().height)/2);

        initActions();
        JPanel bPane = new JPanel(new GridLayout(3,1,3,3));
        mainPane.add(bPane,BorderLayout.SOUTH);

        toolbar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolbar.setFloatable(false);
        toolbar.addSeparator();
        toolbar.add(actPlay);
        toolbar.add(actStop);
        toolbar.add(actLoad);
        setSudoku();
     
        // slider
        slider.setOrientation(SwingConstants.HORIZONTAL);
        slider.setMinimum(0);
        slider.setMaximum(2000);
        slider.setValue(1000);
        slider.addChangeListener(
        		new ChangeListener() {
					@Override
					public void stateChanged(ChangeEvent e) {
						int value = slider.getValue();
						// min delay is 100 ms
						timer.setDelay(Math.max(10,value));
					}
        		}
        );
        slider.setMajorTickSpacing(500);
        slider.setMinorTickSpacing(100);
        slider.setPaintLabels(true);
        slider.setPaintTicks(true);
      
        JPanel sPane = new JPanel(new GridLayout(1,2,5,5));
        sPane.setBorder(BorderFactory.createEmptyBorder(0,5,0,10));
        sPane.add(new JLabel("animation speed (delay in ms):",JLabel.RIGHT));
        sPane.add(slider);
        bPane.add(sPane);
        sPane = new JPanel(new GridLayout(1,2,5,5));
        sPane.setBorder(BorderFactory.createEmptyBorder(0,5,0,10));
        sPane.add(new JLabel("computation steps:",JLabel.RIGHT));
        bPane.add(sPane);
        bPane.add(toolbar);
        
    }

    private void initActions() {
    	ActionListener l = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (animationOn) {
					  Snapshot next = futoshiki.getNext(snapshot);
					  updateDisplay(next);
				}
				else {
					timer.stop();
				}
			};					
		};
		timer = new Timer(1000,l);
		
		actPlay = new AbstractAction("play") {			

			public void actionPerformed(ActionEvent e) {				
				//switchAnimation(true);
				timer.start(); 	
				animationOn=true;
				}
		};
		actStop = new AbstractAction("stop") {
			public void actionPerformed(ActionEvent e) {				
				  //switchAnimation(false);
				  timer.stop();
				  animationOn=false;
			}
		};
		actLoad = new AbstractAction("load") {

			public void actionPerformed(ActionEvent e) {		
				JFileChooser chooser = new JFileChooser();
			    try {
			    	chooser.setCurrentDirectory(new File("."));
			    }
			    catch (Exception x){}
			    int returnVal = chooser.showOpenDialog(App.this);
			    if(returnVal == JFileChooser.APPROVE_OPTION) {
			       File file = chooser.getSelectedFile();
			       Snapshot s=futoshiki.loadFile(file);
			       if(s==null){
			    	   JOptionPane.showMessageDialog(null, "the format is not correct");
			       }else{
			    	   updateDisplay(s);
			       }
			       
			    }
			}
		};
	}
    
    private void updateDisplay(Snapshot snapshot) {
	
		this.snapshot = snapshot;
		Iterator<Cell> cells = snapshot.cells();
		while (cells.hasNext()) {
			Cell next = cells.next();
			String v=null;
			if(next.getValue().equals("0")){
				v="  ";
			}else if(!next.getValue().equals("null")){
				v=next.getValue();
			}
			(currentLabels[next.getCol()-1][next.getRow()-1]).setText(v);
		} 
	}
    
    private void clearDisplay(){
    	for(int i=0;i<9;i++){
    		for(int j=0;j<9;j++){
    			currentLabels[i][j].setText("  ");
    		}
    	}
    }
	                                              

	@SuppressWarnings("unchecked")
	@Override
	public Object addingService(ServiceReference reference) {
		Contrast service =context.getService(reference);
		timer.stop();
		synchronized (this) {
			setFutoshiki(service);
		}
		return service;
	}

	@Override
	public void modifiedService(ServiceReference arg0, Object arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removedService(ServiceReference reference, Object arg1) {
		 context.ungetService(reference);
		 System.out.println("Display Service for FUTOSHIKI is gone");
		 timer.stop();
		 synchronized (this) {
			 setSudoku();//set back to sudoku display
		 }
	}

}

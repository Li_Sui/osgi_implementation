package nz.ac.massey.cs.sudoku.activator;

import nz.ac.massey.cs.app.App;
import nz.ac.massey.cs.contrast.Contrast;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class SudokuActivator implements BundleActivator{
	
	private ServiceTracker<?, ?> serviceTracker;
	private App app;
	
	@SuppressWarnings("unchecked")
	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("starting sudoku service");
		
	    app=new App(context);
    	java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	app.setVisible(true);
            }
        });
    	
		serviceTracker = new ServiceTracker<Object, Object>(context, Contrast.class.getName(),app);
		serviceTracker.open();
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println("sudoku  is gone");
		java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	app.setVisible(false);
            }
        });
    	
		serviceTracker.close();
		
	}

}

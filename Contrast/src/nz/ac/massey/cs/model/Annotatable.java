package nz.ac.massey.cs.model;

import java.util.*;

public class Annotatable {
	private Map<String,Object> properties = new HashMap<String,Object>();
	public void setProperty(String name,Object value) {
		this.properties.put(name,value);
	}
	public Object getProperty(String name) {
		return this.properties.get(name);
	}
}

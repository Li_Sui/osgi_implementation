package nz.ac.massey.cs.sudoku;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import nz.ac.massey.cs.contrast.Contrast;
import nz.ac.massey.cs.model.Cell;
import nz.ac.massey.cs.model.Snapshot;
import nz.ac.massey.cs.sudoku.persistence.SudokuReader;

/**
 * 
 * @author Li Sui
 *
 */
public class Sudoku implements Contrast{
	
	private JPanel pane;
	private JLabel[][] labels = new JLabel[9][9];
	
	public Snapshot getNext(Snapshot arg0) {
		Iterator< Cell> emptyCell=arg0.unsolvedCells();
		@SuppressWarnings("unchecked")
		Vector<Integer>[][] possibleValue=new Vector[9][9];
      
		//initiate coordinate of Sudoku, start from point(0,0) to(8,8)
        for(int i=1;i<10;i++){
			for(int j=1;j<10;j++){
				  possibleValue[i-1][j-1]=new Vector<Integer>(); // create an array in each coordinate 				
			}
		}
        // iterate empty cell and find possible values
        while(emptyCell.hasNext()){
            Cell temp = emptyCell.next();
			int RowNo=temp.getRow();
			int ColNo=temp.getCol();
		
			for(int k=1;k<10;k++){
				boolean findPvalue=true;
				Iterator<Cell> iterateRow=arg0.cellsByRow(RowNo);
				Iterator<Cell> iterateCol=arg0.cellsByColumn(ColNo);
				
				//iterate each row
			   while(iterateRow.hasNext()){
			    	Cell row = iterateRow.next();
			    	if(!row.getValue().equals("0") && row.getValue().equals(String.valueOf(k))){// if cell in row has value and the value is k which we comparing with 
			    		                                       //statement is true, but findPossibleValue is false
			    		findPvalue=false;// not found
			    	}
			    }
			   //iterate each col
			    while(iterateCol.hasNext()){
			    	Cell col = iterateCol.next();
			    	if(!col.equals("0") && col.getValue().equals(String.valueOf(k))){// if cell in col has value and the value is k which we comparing with
			    		                                       //statement is true, but findPossibleValue is false
			    		findPvalue=false;// not found
			    	}
			    		
			    }
			    
			    
			    // iterate each group
			 if(ColNo<4&&RowNo<4){// if the empty cell in group(1,1) 
				 Iterator<Cell> iterateGroup=arg0.cellsByGroup(1, 1);
				    while(iterateGroup.hasNext()){
				    	Cell group11= iterateGroup.next();
				    	if(!group11.getValue().equals("0") && group11.getValue().equals(String.valueOf(k))){// if cell in group(1,1) has value and the value is k which we comparing with
				    		findPvalue=false;// not found
				    	}
			      }
			 }
			 if(ColNo<4&&RowNo>3&&RowNo<7){
				 Iterator<Cell> iterateGroup=arg0.cellsByGroup(1, 2);
				    while(iterateGroup.hasNext()){
				    	Cell group12= iterateGroup.next();
				    	if(!group12.getValue().equals("0") && group12.getValue().equals(String.valueOf(k))){
				    		findPvalue=false;
				    	}
				    }
			 }
			 if(ColNo<4&&RowNo>6){
				 Iterator<Cell> iterateGroup=arg0.cellsByGroup(1, 3);
				    while(iterateGroup.hasNext()){
				    	Cell group13= iterateGroup.next();
				    	if(!group13.getValue().equals("0")&& group13.equals(String.valueOf(k))){
				    		findPvalue=false;
				    	}
				    }
			 }
			 if(ColNo>3&&ColNo<7&&RowNo<4){
				 Iterator<Cell> iterateGroup=arg0.cellsByGroup(2, 1 );
				    while(iterateGroup.hasNext()){
				    	Cell group21= iterateGroup.next();
				    	if(!group21.getValue().equals("0") && group21.getValue().equals(String.valueOf(k))){
				    		findPvalue=false;
				    	}
				    }
			 }
			 if(ColNo>3&&ColNo<7&&RowNo>3&&RowNo<7){
				 Iterator<Cell> iterateGroup=arg0.cellsByGroup(2, 2);
				    while(iterateGroup.hasNext()){
				    	Cell group22= iterateGroup.next();
				    	if(!group22.getValue().equals("0") && group22.getValue().equals(String.valueOf(k))){
				    		findPvalue=false;
				    	}
				    }
			 }
			 if(ColNo>3&&ColNo<7&&RowNo>6){
				 Iterator<Cell> iterateGroup=arg0.cellsByGroup(2, 3);
				    while(iterateGroup.hasNext()){
				    	Cell group23= iterateGroup.next();
				    	if(!group23.getValue().equals("0") && group23.getValue().equals(String.valueOf(k))){
				    		findPvalue=false;
				    	}
				    }
			 }
			 if(ColNo>6&&RowNo<4){
				 Iterator<Cell> iterateGroup=arg0.cellsByGroup(3, 1);
				    while(iterateGroup.hasNext()){
				    	Cell group31= iterateGroup.next();
				    	if(!group31.getValue().equals("0") && group31.getValue().equals(String.valueOf(k))){
				    		findPvalue=false;
				    	}
				    }
			 }
			 if(ColNo>6&&RowNo>3&&RowNo<7){
				 Iterator<Cell> iterateGroup=arg0.cellsByGroup(3, 2);
				    while(iterateGroup.hasNext()){
				    	Cell group32= iterateGroup.next();
				    	if(!group32.getValue().equals("0") && group32.getValue().equals(String.valueOf(k))){
				    		findPvalue=false;
				    	}
				    }
			 }
			 if(ColNo>6&&RowNo>6){
				 Iterator<Cell> iterateGroup=arg0.cellsByGroup(3, 3);
				    while(iterateGroup.hasNext()){
				    	Cell group33= iterateGroup.next();
				    	if(!group33.getValue().equals("0") && group33.getValue().equals(String.valueOf(k))){
				    		findPvalue=false;
				    	}
				    }
			 }
			 			   
			    if(findPvalue==true){// find possible values
			    	possibleValue[ColNo-1][RowNo-1].addElement(k); //add it into possibleValue
			    }
			}
		}
        // check if there is only one possible values in particular cell
        Iterator< Cell> check=arg0.unsolvedCells();
        while(check.hasNext()){
        	Cell temp2 = check.next();
			int RowNo2=temp2.getRow();
			int ColNo2=temp2.getCol();
				if(possibleValue[ColNo2-1][RowNo2-1].size()==1){// if we find there is only one possible value 
					for(int i=0;i<possibleValue[ColNo2-1][RowNo2-1].size();i++)
						if(possibleValue[ColNo2-1][RowNo2-1]!=null){
					      temp2.setValue(String.valueOf(possibleValue[ColNo2-1][RowNo2-1].get(i)));// we set it in according cell
						}
			}
       }
        
		return arg0;
	}

	@Override
	public JPanel changeBoardLayout() {
		   pane=new JPanel();
		   pane.setOpaque(true);
	       pane.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
	       pane.setBackground(Color.WHITE);
			pane.setLayout(new java.awt.GridLayout(9, 9));
	        for (int i=0;i<9;i++) {
	        	for (int j=0;j<9;j++) {
	        		JPanel p = new JPanel();
	        		p.setOpaque(false);
	        		p.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
	                JLabel l = new JLabel();
	                l.setOpaque(false);
	                l.setHorizontalAlignment(SwingConstants.HORIZONTAL);
	                l.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
	                p.add(l);        		
	               
	                labels[i][j] = l;
	                pane.add(p);
	        	}
	        }
			return pane;
	}

	@Override
	public JLabel[][] changeLabel() {
		
    	for(int i=0;i<9;i++){
    		for(int j=0;j<9;j++){
    			labels[i][j].setText("  ");
    		}
    	}
		return this.labels;
	}

	@Override
	public Snapshot loadFile(File file) {
		SudokuReader r =new SudokuReader();
		Snapshot s=null;
		try {
			if(r.check(file)){
				s=r.read(file);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return s;
	}
}

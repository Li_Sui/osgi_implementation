 How to deploy:
 
1. import to eclipse
2. select run configuration
3. under bundles tag, select bundle you want install, then select these required bundles
			- org.eclipse.osgi
			- org.eclipse.equinox.console
			- org.apache.felix.gogo.runtime
			- org.apache.felix.gogo.shell
			
equinox console command:  ss  ----list all bundles
						  start [id] ---start a bundle
						  stop [id]  ----stop a bundle
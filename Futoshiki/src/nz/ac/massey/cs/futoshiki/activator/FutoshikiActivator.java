package nz.ac.massey.cs.futoshiki.activator;


import nz.ac.massey.cs.contrast.Contrast;
import nz.ac.massey.cs.futoshiki.Futoshiki;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

public class FutoshikiActivator implements BundleActivator{
	@SuppressWarnings("rawtypes")
	private ServiceRegistration registration;
	private BundleContext context;
	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("starting FUTOSHIKI");
		this.context=context;
		 registration = this.context.registerService(
	               		Contrast.class.getName(),
	                new Futoshiki(),
	                null);

		
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		context=null;
		registration.unregister();
	}

}
